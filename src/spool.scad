include <lib/parameters.scad>
use <lib/util.scad>
use <lib/gear_util.scad>

spool_outer_small();
module spool_outer_small(){
  small_r = Spool_r/4;
  difference(){
    union(){
      // Edge to keep line in place
      cylinder(r = Sep_disc_radius, h = 1, $fn=100);
      cylinder(r = small_r, h = Spool_height+1+Torx_depth-0.1, $fn=150);
    }
    for(v=[0:120:359])
      rotate([0,0,v + 30])
        translate([0, small_r/2, 1+Spool_height/2])
          rotate([-90,00,0]){
            for(i=[-2.2,2.2])
              translate([i,0,0])
                cylinder(r=1.2, h=Spool_r);
            translate([-7/2,-3/2,0])
              cube([7,3,5]);
          }
    translate([0,0,-1])
      cylinder(d1=b608_outer_dia+2.5,d2=b608_outer_dia - 1, h=3,$fn=150);
    translate([0,0,-1])
      cylinder(d=b608_outer_dia, h=(Spool_height + 1)+Torx_depth+1,$fn=150);
  }
}

//spool_outer();
module spool_outer(spools = 1){
  difference(){
    union(){
      // Edge to keep line in place
      cylinder(r = Sep_disc_radius, h = 1, $fn=100);
      cylinder(r = Spool_r, h = Spool_height+1, $fn=150);
      for(s=[0:spools-1])
        if(s > 0)
          translate([0,0, 1 + s*(Spool_height + 1)])
            cylinder(r = Spool_r, h = Spool_height, $fn=150);
      translate([0,0,0.1]) // Sink  (0.5 - 0.1) = 0.4 mm back to make extra space for torx
        torx(h=Torx_depth+spools*(Spool_height + 1) - 0.5, female=false);
    }
    for(s=[0:spools-1]){
      for(v=[0:120:359])
        rotate([0,0,v + 30*s + 30])
          translate([0, Spool_r/2, 1+Spool_height/2 + s*(1 + Spool_height)])
            rotate([-90,00,0])
              for(i=[-2.2,2.2])
                translate([i,0,0])
                  cylinder(r=1.2, h=Spool_r);
    }
    translate([0,0,-1])
      cylinder(d=b608_outer_dia+2.5, h=3,$fn=150);
    translate([0,0,-1])
      cylinder(r=Spool_r-Spool_outer_wall_th, h=spools*(Spool_height + 1)+Torx_depth+1,$fn=150);
  }
}

//color([0.4,0.75,0.4]) import("../stl/spool.stl");
spool();
module spool(){
  spool_outer();
  spool_center();
}
