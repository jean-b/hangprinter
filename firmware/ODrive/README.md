## ODrive Usage On Hangprinter

Use stock ODrive Firmware.

The source is kept here:
[https://github.com/madcowswe/ODrive](github.com/madcowswe/ODrive).

Other file(s) in this directory are ther to help your manual configuration process of the ODrive.
Read every line, including the comments manually before executing any of them.

[The official ODrive docs](https://docs.odriverobotics.com/) are helpful in explaining many of the commands.
